if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const express = require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const cors = require('cors')

const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URI);

const devices = require('./routes/devices')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

app.use(cors())

app.use('/api/devices', devices)

module.exports = app
