const Device = require('../models/devices')

const getAllDevices = async (req, res) => {
    try {
        const devices = await Device.find({})

        res.json({
            devices
        })
    } catch (e) {
        console.log({
            e
        })
        res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}

const getDevice = async (req, res) => {
    const { id } = req.params
    try {
        const device = await Device.findOne({
            _id: id
        })

        if (!device) {
            res.status(400).json({
                error: 'Device not found'
            })
            return
        }

        res.json({
            device
        })
    } catch (e) {
        console.log({
            e
        })
        res.status(500).json({
            error: e.message || 'Internal Server Error'
        })
    }
}

const createDevice = async (req, res) => {
    const { body } = req

    try {
        const device = await Device.create(body)

        res.json({
            device
        })
    } catch (e) {
        res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}

const updateDevice = async (req, res) => {
    const { id } = req.params
    const { body } = req

    try {
        let device = await Device.findOne({
            _id: id
        })

        if (!device) {
            res.status(400).json({
                error: 'Device not found'
            })
            return
        }

        device = await Device.findOneAndUpdate({
            _id: id
        }, body, {
                new: true
            })

        res.json({
            device
        })
    } catch (e) {
        res.status(500).json({
            error: e.message || 'Internal Server Error'
        })
    }
}

const deleteDevice = async (req, res) => {
    const { id } = req.params

    try {
        const device = await Device.findOne({
            _id: id
        })

        if (!device) {
            res.status(400).json({
                error: 'Device not found'
            })
            return
        }

        await Device.remove({
            _id: id
        })

        res.json({
            success: true
        })
    } catch (e) {
        res.status(500).json({
            error: e.message || 'Internal Server Error'
        })
    }
}


module.exports = {
    getAllDevices,
    getDevice,
    createDevice,
    updateDevice,
    deleteDevice
}